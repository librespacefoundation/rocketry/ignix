<img src="/media/Media_Pack.png" alt="drawing" width="500"/>


# IGNiX - Solid Rocket Motor

This is IGNiX, a Solid Rocket Motor using Sorbitol based propellants currently under the Research and Design phase.

## Project Overview

IGNiX is a simple M-Class Solid Rocket Motor using KSNB propellant designed with reliability and ease of manufacturing in mind to be made and used by university student teams and hobbyists.

## License

This project is licensed under the CERN OHL v1.2

## Acknowledgments

- Developed by: Petros Tsiompanis, Thanos Patsas
- Inspired by the passion for rocketry and open-source development.

## Contact

For any questions or feedback, please [reach out](mailto:ignix@tsiompanis.com).
